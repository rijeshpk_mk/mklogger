<?php
/*
 * Author: Rijesh
 */

namespace MkLogger;
use Monolog\Logger;
use Monolog\Handler\MongoDBHandler;
use GuzzleHttp\Client;


class MkLogger{
    public $MONGO_HOST;
    public $MONGO_PORT;
    public $MONGO_DB;
    public $MONGO_COLLECTION = "log";
    public $LOG_API_URL = "";
    public $ACCESS_TOKEN = "";
    /*
     * contruct
     */
    function __construct($configs = array()){
        if(isset($configs['mongo_host'])) $this->MONGO_HOST = $configs['mongo_host'];
        if(isset($configs['mongo_port'])) $this->MONGO_PORT = $configs['mongo_port'];
        if(isset($configs['mongo_db'])) $this->MONGO_DB = $configs['mongo_db'];
        if(isset($configs['mongo_collection'])) $this->MONGO_COLLECTION = $configs['mongo_collection'];
        if(isset($configs['log_api_url'])) $this->LOG_API_URL = $configs['log_api_url'];
        if(isset($configs['access_token'])) $this->ACCESS_TOKEN = $configs['access_token'];
    }

    /*
     * Use it only if it is really required
     * array('user' => array('id'=>1, 'name' =>'xxx','email' => '')
     *      'log_entity' => 'product',
     *      'log_entity_key' => 123,
     *      'log_event' => 'ADD',
     *      'log' => Array(Mixed)
     * )
     */
    public function logDirect($aLog =  array()){
        //$logInput = array('user'=>'testfoooooo','log_type' =>'product', 'log_key' => 1212, 'log'=>'test','event' => "Add");

        if($this->_validate($aLog)) {
            $handler = new MongoDBHandler(
                new \MongoClient('mongodb://'.$this->MONGO_HOST.':'.$this->MONGO_PORT),
                $this->MONGO_DB,
                $this->MONGO_COLLECTION
            );
            $log = new Logger('audit');
            $log->pushHandler($handler);
            $logData = array(
                "datetime" => new \MongoDate(),
                "user" => $aLog['user'],
                "log_entity" => $aLog['log_type'],
                "log_entity_key" => $aLog['log_key'],
                "log" => $aLog['log'],
                "log_event" => $aLog['event']
            );
            $logData = json_encode($logData);
            $log->addInfo($logData);
            $success = 1;
            $error = '';
        }else{
            $error = array('Missing required params: user , log_type, log_key, log, event');
            $success = 0;
        }
        return array("success" => $success, 'error' => $error);
    }


    /*
     * Logger post Sevice
     * array('user' => array('id'=>1, 'name' =>'xxx','email' => '')
     *      'log_entity' => 'product',
     *      'log_entity_key' => 123,
     *      'log_event' => 'ADD',
     *      'log' => Array(Mixed)
     * )
     */
    public function logAsynch($aLog){
        if($this->_validate($aLog)) {
            $client = new \GuzzleHttp\Client();
            $promise = $client->postAsync($this->LOG_API_URL, [
                'json' => $aLog, 'headers'  => [
                    'x-access-token' => $this->ACCESS_TOKEN
                ]
            ]);
            $promise->then(function ($response) {
                //$response = json_decode($response->getBody(), true);
            });
            $promise->wait();
            $success = 1;
            $error = '';

        }else{
            $error = array('Missing required params: user , log_entity, log_entity_key, log, log_event');
            $success = 0;
        }

        return array("success" => $success, 'error' => $error);
    }
    /*
     * Logger get service
     * Filters : log_entity , log_entity_key
     */
    public function getLogs($aFilter = array()){
        $client = new \GuzzleHttp\Client();
        $response = $client->get($this->LOG_API_URL, [
            'query' => $aFilter, 'headers'  => [
                'x-access-token' => $this->ACCESS_TOKEN,
            ]
        ]);
        return array("success" => 1, 'response' => $response->getBody()->getContents());
    }

    protected function _validate($logInput){
        if(!isset($logInput['user']) || !isset($logInput['log_entity']) ||
            !isset($logInput['log_entity_key']) || !isset($logInput['log']) || !$logInput['log_event']) {
            return false;
        }
        return true;
    }
}